CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuring the Back-End
* Styling the Front-End


INTRODUCTION
------------

Current Maintainer: Vladimir Kosenko <volodymyr.kosenko@cocomore.com>

The XML3D Module provides a simple and easy way to integrate the XML3D models
and applications into Drupal. The current 7.x-1.x version allows to simply
enter the XML3D code into a Long Text field that has the XML3D Input Field
formatter. The content will then be displayed on the page in the form of
an iframe having the same size as the XML3D application.


REQUIREMENTS
------------

The module has the following requirements:

* Drupal 7
* The field, field_ui, libraries modules
* A browser with Web GL support (currently, Firefox and Chrome)
* The js library files (xml3d.js: http://www.xml3d.org/xml3d/script/xml3d.js
  and camera.js: http://www.xml3d.org/xml3d/script/tools/camera.js)
  must be located in the corresponding folder in the libraries directory
  (generally, under the following path: sites/all/libraries/xml3d/).


INSTALLATION
------------

1. Place the xml3d folder into the modules folder of your site (generally,
   under sites/all/modules/contrib/).

2. Download the library files from the official XML3D web site
   (the current version of the module uses XML3D version 4.3):
   http://www.xml3d.org/xml3d/script/xml3d.js,
   http://www.xml3d.org/xml3d/script/tools/camera.js and put them into
   the project folder in the libraries directory (generally, under:
   sites/all/libraries/xml3d).

3. Here is an example of how your folder structure would look like:
   sites
   |--all
      |--libraries
         |--xml3d
            |--xml3d.js
            |--camera.js
      |--modules
         |--contrib
            |--xml3d
               |--css
                  |--xml3d.css
               |--examples
                  |--css_transforms.txt
                  |--rubik_cube.txt
                  |--rubik_cube_interactive.txt
               |--README.txt
               |--xml3d.info
               |--xml3d.install
               |--xml3d.tpl.php

4. Install the module either via the web interface or the command line tool.


CONFIGURING THE BACK-END
------------------------

The current version of the module allows only one way to add your XML3D code:
paste it into a Long Type text field in your content type.

The Long Type text field must have the "XML3D Input Field" formatter (you
can do that by going to: Structure -> Content types -> Display Fields).

In order to add/modify your XML3D content you just need to go to your content
type that contains a field with the XML3D formatter and paste the content into
the field (you can grab the code from one of the examples available at
http://xml3d.github.com/xml3d-examples/).

Please, pay attention to the fact that the current version of the module
supports content without any external links (either code or images). Thus,
the module folder contains an example directory with three sample files:
css_transforms.txt, rubik_cube.txt and rubik_cube_interactive.txt.
You can just copy the content of these files and insert it into you field.


STYLING THE FRONT-END
---------------------

The XML3D element will be presented as an iframe that will have the size of
the content inside (the width and height parameters of the <xml3d> element
will be used for this). By default, the iframe has neither borders nor
scrolling. That can easily be changed via the CSS file in the module
([modules folder]/xml3d/css/xml3d.css). The iframe has a class named
"xml3d_frame" that you can use for styling it.
