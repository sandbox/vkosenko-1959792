<?php
/**
 * @file
 * This is the template file for the iframe content.
 */

?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>XML3D Content</title>
    <script type="text/javascript">if(window.self === window.top){window.top.location.href = "/";}</script>
    <?php
      echo $script;
    ?>
    <style type="text/css" media="all">
      xml3d {
        display: none;
      }
    </style>
  </head>
  <body>
    <?php echo $content; ?>
  </body>
</html>
